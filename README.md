# oops-framework

#### 介绍
Cocos Creato 3.x 版本的游戏框架，框架中集成一些优秀的第三方库，基本游戏开发常用的功能都支持

#### 基础功能
1. 资源管理
2. 音乐管理
3. 界面管理
    - 屏幕自适应
    - 主界层
    - 弹窗层
    - 模式弹窗层
    - 系统消息层
    - 数据驱动加载界面
4. 多语言管理
    - 文本多语言组件
    - 图片多语言组件
5. 网络模块
    - HttpRequest
6. 全局消息管理
7. 随机数生成器
8. 定时器管理

#### 工具库
1. 异步队列
2. 日志管理 
3. 字符串处理
4. 基础数学库
5. 常用UI组件

#### 集成优秀的第三方库
1. WebSocket 框架（原创者设计灵活，易扩展自定义协议）[原创者代码地址](https://github.com/wyb10a10/cocos_creator_framework)
2. animator 可视化动画状态机（与Untiy3D的Animator使用体验类似，扩展三维骨骼动画支持） [原创者代码地址](https://github.com/LeeYip/cocos-animator)
3. model-view MVVM组件库（原库为cc2.x版本，本人升级到cc3.x） [原创者代码地址](https://github.com/wsssheep/cocos_creator_mvvm_tools)
4. seedrandom 带随机种子的随机数库 [原创者代码地址](https://www.npmjs.com/package/seedrandom)
5. ECS 框架 [原创者代码地址](https://github.com/shangdibaozi/ECS)
6. 资源打包zip加载 [原创者代码地址](https://github.com/Stuk/jszip)
7. behavior-tree 行为树
8. 本地存储库数据加密
9. MD5、RSA、Crypto